from __future__ import unicode_literals

from django.db import models
from .validators import validate_description

from django.utils.translation import pgettext_lazy
from django.utils.translation import ugettext_lazy as _

# Create your models here.
class Item(models.Model):
    title = models.CharField(max_length=200)
    # title = models.CharField(max_length=200,help_text=pgettext_lazy('help text for MyThing model', 'This is the help text'))
    description = models.TextField()
    # description = models.TextField(_('description !'),help_text=_('This is the help text'))
    amount = models.IntegerField(max_length=6)
    field4 = models.TextField(null=True, blank=True)
    field5 = models.TextField(default="default_5")
    SIZE = (
        ('S', 'Small'),
        ('M', 'Medium'),
        ('L', 'Large'),
    )
    field7 = models.CharField(max_length=2, choices=SIZE, default='X')

    def _get_full_name(self): # The last method in this example is a property.
        "Returns the person's full name."
        return 'Get_full_name title::%s description::%s' % (self.title, self.description)
    full_name = property(_get_full_name)

    def __unicode__(self):
        return self.title

class Knight(models.Model):
    name = models.CharField(max_length=100)
    of_the_round_table = models.BooleanField()
    old = models.IntegerField(null=True)
    def __unicode__(self):
        return self.name
class Employee(models.Model):
    firstName = models.CharField(max_length=100)
    lastName = models.CharField(max_length=100)


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    
    def __str__(self):
        return self.question_text
    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days = 1)
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True 
    was_published_recently.short_description = 'Published recently?'


class Choice(models.Model):
    question = models.ForeignKey(Question)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    def __str__(self):
        return self.choice_text


class Person(models.Model):
    name = models.CharField(max_length=128)
    last_name = models.CharField(max_length=50, null=True, blank=True)
    birth_date = models.DateField(null=True, blank=True)

    def baby_boomer_status(self):
        "Returns the person's baby-boomer status."
        import datetime
        if self.birth_date < datetime.date(1945, 8, 1):
            return "Pre-boomer"
        elif self.birth_date < datetime.date(1965, 1, 1):
            return "Baby boomer"
        else:
            return "Post-boomer"

    def _get_full_name(self): # The last method in this example is a property.
        "Returns the person's full name."
        return '%s %s' % (self.first_name, self.last_name)
    full_name = property(_get_full_name)
    def __unicode__(self):
        return self.name

class Group(models.Model):
    name = models.CharField(max_length=128)
    members = models.ManyToManyField(Person, through='Membership')
    def __unicode__(self):
        return self.name

class Membership(models.Model):
    person = models.ForeignKey(Person)
    group = models.ForeignKey(Group)
    date_joined = models.DateField()
    invite_reason = models.CharField(max_length=64)