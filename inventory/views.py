from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.http import Http404
from inventory.models import Item
from inventory.models import Employee
from inventory.forms import ItemForm
from django.http import HttpResponseRedirect
from django.core.files import File

import os
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings

from django.shortcuts import render_to_response
from .forms import UploadFileForm
from .forms import ForgotPassForm

from os.path import join
import json
# import xmltodict, json
import xml.etree.ElementTree
import xml.etree.ElementTree as ET
import xlrd
# from BeautifulSoup import BeautifulSoup
from bs4 import BeautifulSoup
import codecs

from django.contrib.auth.models import User
from django.contrib.auth import authenticate

from django.utils.translation import ugettext as _
from django.utils.translation import pgettext
from django.utils.translation import get_language_info

from django.views.generic.base import TemplateView
from django.views.generic import View
# Create your views here.


def index(request):
    items = Item.objects.all()
    print '1'
    print '2'
    for it in items:
        print "--", it.title
        # import pdb;pdb.set_trace()
    return render(request,'inventory/index.html',{
        'items': items,
    })
    # return HttpResponse('<p>In Index view</p>')

class HomePageView(TemplateView):

    template_name = 'inventory/home.html'

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        context['items'] = Item.objects.all()
        return context

class MyView(View):
    __name__ = 'AddItemsView object' # bodge to stop functools complaining
    def get(self, li, request):
        return HttpResponse('result')
    def __call__(self, li1, request, *args):
        print args # ('a', 'b', 'c')
        return HttpResponse('result - call ' + li1)




def item_detail(request,id):
    print 'item_detail'
    try:
        item = Item.objects.get(id=id)
    except Item.DoesNotExist:
        raise Http404('This item not exist')
    return render(request,'inventory/item_detail.html',{
        'item': item,
    })    
    # return HttpResponse('<p>An item_detail view with id {0}</p>'.format(id))
def index2(request):
    return HttpResponse("Hello 2, world. You're at the polls index.")

def item_add(request):
    if request.method == 'POST': # If the form has been submitted...
        # ItemForm was defined in the previous section
        form = ItemForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            
            #instance = form.save(commit=False)
            #item = Item.objects.get(title='title 3')
            #instance.title = item.description
            #instance.save()

            print request.POST
            title = request.POST.get('title', '')
            description = request.POST.get('description', '')
            amount = request.POST.get('amount', '')
            instance = Item(title=title, description=description, amount=amount)
            instance.save()

            return HttpResponseRedirect('/thanks/') # Redirect after POST
    else:
        form = ItemForm() # An unbound form

    return render(request, 'inventory/item_add.html', {
        'form': form,
    })

def item_add2(request):
    if request.method == 'POST': # If the form has been submitted...
        # ItemForm was defined in the previous section
        form = ItemForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            
            # instance = form.save(commit=False)
            # item = Item.objects.get(title='title 3')
            # instance.title = item.description
            # instance.save()

            print request.POST
            print "Success success"
            title = request.POST.get('title', '')
            description = request.POST.get('description', '')
            amount = request.POST.get('amount', '')
            field4 = request.POST.get('field4_select', '')
            field7 = request.POST.get('field7', '')
            instance = Item(title=title, description=description, amount=amount, field4=field4, field7=field7)
            instance.save()

            return HttpResponseRedirect('/thanks/') # Redirect after POST
    else:
        form = ItemForm() # An unbound form

    return render(request, 'inventory/item_add2.html', {
        'form': form,
    })

def clean_email(request):
    if request.method == 'POST':
        form = ForgotPassForm(request.POST)
        if form.is_valid():
            return HttpResponseRedirect('/thanks/') 
    else:
        form = ForgotPassForm()
    return render(request, 'inventory/clean_email.html', {
        'form': form,
    })
    # player = get_or_none()

def save_item(request):
    response_data = {}
    response_data['result'] = 'fail'
    response_data['message'] = 'Can\'t saved \n'
    if request.method == 'POST':
        form = ItemForm(request.POST)  # A form bound to the POST data
        if form.is_valid():  # All validation rules pass
            title = request.POST.get('title', '')
            description = request.POST.get('description', '')
            amount = request.POST.get('amount', '')
            field4 = request.POST.get('field4_select', '')
            field7 = request.POST.get('field7', '')
            instance = Item(title=title, description=description, amount=amount, field4=field4, field7=field7)
            instance.save()  # chay qua line nay la saved vao db
            response_data['result'] = 'success'
            response_data['message'] = 'Saved'
            return HttpResponse(json.dumps(response_data), content_type="application/json")
        else:
            for key, val in form.errors.iteritems():
                response_data['message'] = response_data['message'] + key + ' ' + val[0]
            return HttpResponse(json.dumps(response_data), content_type="application/json")
    else:
        return HttpResponse(json.dumps(response_data), content_type="application/json")
def thanks(request):
    ip = request.META['REMOTE_ADDR']
    return render(request,'inventory/thanks.html',{
        'ip': ip,
    })

def parser(request):
    form = UploadFileForm()
    return render(request,'inventory/parser.html',{
        'form': form
    })
def parsing(request):
    if request.method == 'POST':
        uploaded_file = request.FILES['parse_file'] # <class 'django.core.files.uploadedfile.InMemoryUploadedFile'>
        filename = uploaded_file.name #  1.json
        ext = filename.split('.')[-1] # json
        if ext == "json" or ext == "cfg" or ext == "xlsx":
            if ext == "json":
                dic = parse_json(uploaded_file)
            form = UploadFileForm(request.POST, request.FILES)
            if form.is_valid():
                handle_uploaded_file(uploaded_file,filename)
                return HttpResponseRedirect('/thanks/')
        # can't occur         
        return render(request,'inventory/parsing.html',{
            'dic': dic,
        })
       
def parse_json(uploaded_file):
    data = uploaded_file.read() # <type 'str'>
    dic = json.loads(data) # <type 'dict'>
    print ">>>>>>>>>>>>dic",dic
    return dic

def handle_uploaded_file(f,filename):
    print settings.BASE_DIR
    try:
        with open(join(settings.BASE_DIR, 'box_upload/' + filename), 'wb') as destination:
            for chunk in f.chunks():
                destination.write(chunk)
    except Exception as e:
        raise e

def scan(request):
    path = join(settings.BASE_DIR, 'box_upload/')  # insert the path to your directory 
    f_list = os.listdir(path) # [1.json,2.json,json.cfg]
    list_json_f = []
    list_xml_f = []
    list_xlsx_f = []
    list_html_f = []
    list_cfg_jsonf = []
    list_cfg_xmlf = []
    for f in f_list:
        ext = f.split('.')[-1]
        ext2 = f.split('.')[-2]
        if ext == 'json':
            list_json_f.append(f) 
        if ext == 'xml':
            list_xml_f.append(f)
        if ext == 'xlsx' and "~" not in f:
            list_xlsx_f.append(f)
        if ext == 'html':
            list_html_f.append(f)

        if (ext == 'cfg' and ext2 == 'json') or (f == 'json.cfg'):
            list_cfg_jsonf.append(f)
        if (ext == 'cfg' and ext2 == 'xml') or (f == 'xml.cfg'):
            list_cfg_xmlf.append(f)

    dict_map = {}
    dict_map.update(map_from_json(list_cfg_jsonf)) # deserialises it
    dict_map.update(map_from_xml(list_cfg_xmlf)) # deserialises it

    list_emp = []
    list_emp = list_emp + list_from_json(list_json_f)
    list_emp = list_emp + list_from_xml(list_xml_f)
    list_emp = list_emp + list_from_xlsx(list_xlsx_f)
    list_emp = list_emp + list_from_html(list_html_f)

    count_saved = save_map(list_emp,dict_map)
    return HttpResponse("%r record saved" % count_saved)

def map_from_xml(list_cfg_xmlf):
    temp_map_xml = {}
    for c in list_cfg_xmlf:
        path_name = open(join(settings.BASE_DIR, 'box_upload/' + c)) # xml.cfg
        tree = ET.parse(path_name)
        root = tree.getroot()
        for node_dic in root:
            temp_map_xml.update({node_dic.tag:node_dic.text})
    return temp_map_xml

def map_from_json(list_cfg_jsonf):
    temp_map = {}
    for c in list_cfg_jsonf:
        path_name = open(join(settings.BASE_DIR, 'box_upload/' + c)) # json.cfg
        temp_map.update(json.load(path_name))
    return temp_map

def list_from_json(list_json_f):
    temp_list = []
    for j in list_json_f:
        path_name = open(join(settings.BASE_DIR, 'box_upload/' + j))
        li = json.load(path_name) # deserialises it
        for emp in li['employees']:
            temp_list.append(emp)
    return temp_list

def list_from_xml(list_xml_f):
    temp_list = []
    for j in list_xml_f:
        path_name = open(join(settings.BASE_DIR, 'box_upload/' + j))
        tree = ET.parse(path_name)
        root = tree.getroot()
        for node_emp in root.findall('employees'):
            emp = {}
            for child in node_emp:
                emp.update({child.tag:child.text})
            temp_list.append(emp)
    return temp_list

def list_from_xlsx(list_xlsx_f):
    temp_list = []
    for j in list_xlsx_f:
        path = join(settings.BASE_DIR, 'box_upload/' + j)
        workbook = xlrd.open_workbook(path)
        worksheet = workbook.sheet_by_index(0)
        offset = 0
        # range(worksheet.nrows) =  [0, 1, 2, 3]
        # range(worksheet.ncols) = [0, 1]
        rows = []
        keys = []
        for i, row in enumerate(range(worksheet.nrows)):
            if i <= offset:  # (Optionally) skip headers
                for j, col in enumerate(range(worksheet.ncols)):
                    keys.append(worksheet.cell_value(i, j))

            else:
                r = []
                for j, col in enumerate(range(worksheet.ncols)):
                    r.append(worksheet.cell_value(i, j))
                rows.append(r)
        for emp in rows:
            temp_list.append({keys[0]:emp[0],keys[1]:emp[1]})

        # print 'Got %d rows' % (len(rows) - offset)
        # print keys
        # print rows
    return temp_list

def list_from_html(list_html_f):
    temp_list = []
    for j in list_html_f:
        path = join(settings.BASE_DIR, 'box_upload/' + j)
        f = codecs.open(path, 'r') # string
        soup = BeautifulSoup(f, 'html.parser')
        for em in soup.find_all('div'):
            temp_list.append(em.attrs)
    return temp_list

def save_map(list_emp,dict_map):
    # [{'Ho': 'John 1', 'Ten': 'Doe 1'}, {'Ho': 'Anna 1', 'Ten': 'Smith 1'}, {'Ho': 'Peter 1', 'Ten': 'Jones 1'}]
    # {Ho': 'firstName', 'Ten': 'lastName'}
    count = 0
    all_emp = []
    for emp in list_emp:
        obj_save = {}
        flag = 0
        for key in emp.keys():
            if key in dict_map:
                key_map = dict_map[key]
                obj_save.update({key_map: emp[key]})
                all_emp.append({key_map: emp[key]})
                flag = 1
        if flag == 1:
            instance = Employee(**obj_save) # keyword argument
            instance.save()
            count += 1

    path = join(settings.BASE_DIR, 'box_upload/file.txt')
    handle1=open(path,'w+')
    handle1.write(str (all_emp) )
    handle1.close()

    return count

def scan_opp(request):
    path = join(settings.BASE_DIR, 'box_upload/')
    scanning = Transform(path)
    scanning.setup()
    scanning.map_from_json()
    scanning.map_from_xml()

    scanning.list_from_json()
    scanning.list_from_xml()
    scanning.list_from_xlsx()
    scanning.list_from_html()
    scanning.save_map()

    return HttpResponse(scanning.show())
    
class Transform:
    def __init__(self, path):
        self.path = path
        self.f_list = os.listdir(path)
        self.list_json_f = []
        self.list_xml_f = []
        self.list_xlsx_f = []
        self.list_html_f = []

        self.list_cfg_jsonf = []
        self.list_cfg_xmlf = []
        self.dict_map = {}
        self.list_emp = []
        self.count = 0

    def setup(self):
        for f in self.f_list:
            ext = f.split('.')[-1]
            ext2 = f.split('.')[-2]
            if ext == 'json':
                self.list_json_f.append(f) 
            if ext == 'xml':
                self.list_xml_f.append(f)
            if (ext == 'cfg' and ext2 == 'json') or (f == 'json.cfg'):
                self.list_cfg_jsonf.append(f)
            if (ext == 'cfg' and ext2 == 'xml') or (f == 'xml.cfg'):
                self.list_cfg_xmlf.append(f)
            if ext == 'xlsx' and "~" not in f:
                self.list_xlsx_f.append(f)
            if ext == 'html':
                self.list_html_f.append(f)

    def map_from_json(self):
        for c in self.list_cfg_jsonf:
            path_name = open(join(settings.BASE_DIR, 'box_upload/' + c)) # json.cfg
            self.dict_map.update(json.load(path_name))
    
    def map_from_xml(self):
        for c in self.list_cfg_xmlf:
            path_name = open(join(settings.BASE_DIR, 'box_upload/' + c)) # xml.cfg
            tree = ET.parse(path_name)
            root = tree.getroot()
            for node_dic in root:
                self.dict_map.update({node_dic.tag:node_dic.text})
    
    def list_from_json(self):
        for j in self.list_json_f:
            path_name = open(join(settings.BASE_DIR, 'box_upload/' + j))
            li = json.load(path_name) # deserialises it
            for emp in li['employees']:
                self.list_emp.append(emp)
    
    def list_from_xml(self):
        for j in self.list_xml_f:
            path_name = open(join(settings.BASE_DIR, 'box_upload/' + j))
            tree = ET.parse(path_name)
            root = tree.getroot()
            for node_emp in root.findall('employees'):
                emp = {}
                for child in node_emp:
                    emp.update({child.tag:child.text})
                self.list_emp.append(emp)
    
    def list_from_xlsx(self):
        for j in self.list_xlsx_f:
            path = join(settings.BASE_DIR, 'box_upload/' + j)
            workbook = xlrd.open_workbook(path)
            worksheet = workbook.sheet_by_index(0)
            offset = 0
            # range(worksheet.nrows) =  [0, 1, 2, 3]
            # range(worksheet.ncols) = [0, 1]
            rows = []
            keys = []
            for i, row in enumerate(range(worksheet.nrows)):
                if i <= offset:  # (Optionally) skip headers
                    for j, col in enumerate(range(worksheet.ncols)):
                        keys.append(worksheet.cell_value(i, j))

                else:
                    r = []
                    for j, col in enumerate(range(worksheet.ncols)):
                        r.append(worksheet.cell_value(i, j))
                    rows.append(r)
            for emp in rows:
                self.list_emp.append({keys[0]:emp[0],keys[1]:emp[1]})

    def list_from_html(self):
        temp_list = []
        for j in self.list_html_f:
            path = join(settings.BASE_DIR, 'box_upload/' + j)
            f = codecs.open(path, 'r') # string
            soup = BeautifulSoup(f, 'html.parser')
            for em in soup.find_all('div'):
                self.list_emp.append(em.attrs)

    def save_map(self):
        # [{'Ho': 'John 1', 'Ten': 'Doe 1'}, {'Ho': 'Anna 1', 'Ten': 'Smith 1'}, {'Ho': 'Peter 1', 'Ten': 'Jones 1'}]
        # {Ho': 'firstName', 'Ten': 'lastName'}
        for emp in self.list_emp:
            obj_save = {}
            flag = 0
            for key in emp.keys():
                if key in self.dict_map:
                    key_map = self.dict_map[key]
                    obj_save.update({key_map: emp[key]})
                    flag = 1
            if flag == 1:
                instance = Employee(**obj_save) # keyword argument
                instance.save()
                self.count += 1
        return self.count
    
    def show(self):
        s = "Can't Save"
        if (self.count > 0):
            s = """ Scan with OOP success:<br/>
                -> list_json_f: %r, <br/>
                -> list_xml_f: %r , <br/>
                -> list_cfg_jsonf: %r, <br/>
                -> list_cfg_xmlf : %r, <br/>
                -> dict_map : %r, <br/>
                -> list_emp : %r, <br/>
                -> count saved : %r, <br/>
            """ % (self.list_json_f,self.list_xml_f,self.list_cfg_jsonf,self.list_cfg_xmlf,self.dict_map,self.list_emp,self.count )
            return s
        return s

def authen(request):
    # user = authenticate(username='john', password='johnpassword') # johnpassword
    # user = authenticate(username='hoangvu', password='2011991') # johnpassword
    user = authenticate(username='admin', password='2011991') # johnpassword

    if user is not None:
        # the password verified for the user
        if user.is_active:
            res = "User is valid, active and authenticated"
        else:
            res = "The password is valid, but the account has been disabled!"
    else:
        # the authentication system was unable to verify the username and password
        res = "The username and password were incorrect."

    return HttpResponse(res)

def profile_see(request):
    return render(request,'inventory/profile_see.html')
def check_login(request):
    if request.user.is_authenticated():
        return HttpResponse("logged in")
    else:
        return HttpResponse("Not authenticate")


# def my_view(request):
def my_view(request):
    # output = _("Welcome to my site.")
    
    # words = ['Welcome', 'to', 'my', 'site.']
    # output = _(' '.join(words))

    # sentence = 'Welcome to my site.'
    # output = _(sentence)

    # Translators: This message appears on the home page only
    # output = _('Today is %(month)s %(day)s.') % {'month': m, 'day': d}
    li = get_language_info('de')
    print(li['name'], li['name_local'], li['bidi'])

    page = _("You're in home") 
    view_param = {
        "page": page
    }
    # return HttpResponse(output)
    return render(request,'inventory/my_view.html',view_param)

def test_ajax(request):
    return render(request,'inventory/test_ajax.html')

def ajax_json(request):
    response_data = {'a':'10','b':'20','c':'30','d':'40','e':'50'}
    res = request.GET.get('callback','') + "(" + json.dumps(response_data) + ");"
    return HttpResponse(res, content_type="application/json")

import math
def test_try(request,x):
    message = None
    print type(x)
    x = int(x)
    print type(x)
    try:
        try:
            x1 = math.sqrt(x)
        except Exception:
            message = "can't take sqrt"
            raise
        try:
            x1 = math.log(x1-4)
        except Exception:
            message = "can't compute log"
            raise
        try:
            x2 = math.arcsin(x1)
        except Exception:
            message = "Can't calculate arcsin"
            raise
    except Exception:
        print message
    return HttpResponse('<p>Test Try</p>')  

def test_try2(request,x):
    message = None
    print type(x)
    x = int(x)
    print type(x)
    try:
        try:
            y = 3/0
            x1 = math.sqrt(x)
            message = "can't take sqrt"
            x1 = math.log(x1-4)
            message = "can't compute log"
            x2 = math.arcsin(x1)
        except ValueError:
            message = "Can't calculate arcsin"
            print '==> Exception 1.1 ValueError'
            # raise
        except ZeroDivisionError:
            print '==> Exception 1.2 ZeroDivisionError'
            raise
    except Exception:
        print '==> Exception 2 Exception'
        print message
    return HttpResponse('<p>Test Try</p>')        