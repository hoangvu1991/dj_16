from django.contrib import admin
from django.core.urlresolvers import reverse
# Register your models here.
from .models import Item
from .models import Knight
from django.db import models
from django.contrib import admin

from inventory.forms import ItemForm
from inventory.forms import ItemAdmin
from inventory.models import Item

class ItemAdmin(admin.ModelAdmin):
    list_display = ['title','description','amount']
    readonly_fields = ("show_url",)
    def show_url(self, instance):
        # url = reverse("ice_cream_bar_detail", kwargs={"pk": instance.pk})
        url = reverse('admin:app_list', kwargs={'app_label': 'auth'})
        response = """<a href="{0}">{0}</a>""".format(url)
        return response
    show_url.short_description = 'Ice Cream Bar URL"'
    show_url.allow_tags = True
    # fieldsets = (
    #     (None, {
    #         'fields': ('amount',)
    #     })
    # )

    # !!
    # formfield_overrides = {
    #     models.TextField: {'widget': ItemAdmin},
    # }
    form = ItemForm

class KnightAdmin(admin.ModelAdmin):
    list_display = ['name','of_the_round_table','old']

admin.site.register(Item,ItemAdmin)
admin.site.register(Knight,KnightAdmin)
