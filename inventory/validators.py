from django.core.exceptions import ValidationError
def validate_description(value):
    """ Raise a ValidationError if the
        value doesn't start with the
        word 'Des:'
    """
    if not value.startswith(u"Des:"):
        msg = u"_Must start with Des:"
        raise ValidationError(msg)