from django import forms
from django.contrib import admin
from inventory.models import Item
from inventory.models import Knight
from django.core.exceptions import ValidationError
from .validators import validate_description

# class ItemForm(forms.Form):
class ItemForm(forms.ModelForm):
    
    def __init__(self, *args, **kwargs):
        # Call the original __init__ method before assigning
        # field overloads
        super(ItemForm, self).__init__(*args,**kwargs)
        self.fields["title"].required = False
        self.fields["description"].required = True
        self.fields["description"].validators.append(validate_description)

    
    class Meta:
        model = Item
        fields = ['title', 'description', 'amount', 'field4', 'field7']
    
    # ... Don't do this! Duplication of the model field!
    # amount = forms.CharField()
    field4_select = forms.ModelChoiceField(queryset=Knight.objects.all(), empty_label="--",to_field_name="name")
    
    def clean(self):
        cleaned_data = super(ItemForm, self).clean()
        data_title = cleaned_data.get("title", "")
        if (data_title == '1'):
            raise forms.ValidationError("_Title invalid.")
        return cleaned_data

    def clean_amount(self):
        # do something that validates your data
        data_amount = self.cleaned_data['amount'] 
        # khi validate mac dinh qua, no moi xuong day valid lan nua
        if data_amount > 10:
            raise forms.ValidationError("_You must input < 10")
        return data_amount

class ItemAdmin(admin.ModelAdmin):
    exclude = ['title']
    form = ItemForm

class UploadFileForm(forms.Form):
    parse_file  = forms.FileField()

class ForgotPassForm(forms.Form):
    email = forms.EmailField(max_length=16, required=True, 
        widget=forms.TextInput(attrs={'placeholder':'Email','class': 'input-block-level'}), 
            error_messages={'required': 'Please input a email'}
                )
    
    # rating = forms.ChoiceField(choices=STARS, error_messages={'required': 'Please choose a star rating'})
    #def clean(self):
    #    print "==>clean", form_data.get('email') #['email']
    #    form_data = self.cleaned_data
    #    if form_data['email'] == '':
    #        raise forms.ValidationError("111!")
    #    return form_data
    # class Meta:
    #     error_messages = {
    #         'email': {
    #             'required': "Please input",
    #         },
    #     }
    def clean(self):
        cleaned_data = super(ForgotPassForm, self).clean()
        email = cleaned_data.get("email")
        if email:
            if "VV" not in email:
                raise forms.ValidationError("Phai co VV")
        # print "-----------",form_data.get('email')
        # return form_data
    # def clean_email(self):
    #     print "==>clean email"
    #     data_email = self.cleaned_data['email'] # 212@gm.com
    #     # khi validate mac dinh qua, no moi xuong day valid lan nua
    #     #if data_email != ' ':
    #     #    raise forms.ValidationError("You must input email")
    #     if "fred@example.com" not in data_email:
    #         # raise forms.ValidationError('email not exist', code='invalid')
    #         raise forms.ValidationError("You have forgotten about Fred!")
    #     return data_email