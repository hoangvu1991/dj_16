from functools import wraps

def dispatch_view(v):
    """Dispatch a request to a dispatch_view that takes a Handler instance as its
    first argument"""

    @wraps(v)
    def _inner(request, *args, **kwargs):
        li = 'aaa'
        return v(li, request, 'a', 'b', 'c', **kwargs)

    # Slightly ugly emergency hack for
    # generic_scheme.views.api.AddItemsView:
    if getattr(v, 'csrf_exempt', False):
        _inner.csrf_exempt = True
    return _inner
