from django.test import TestCase
from inventory.models import Employee
from inventory.views import Transform
from os.path import join
from django.conf import settings
from django.utils import unittest

# Create your tests here.

class TransformMethodTests(unittest.TestCase):
    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')
    def test_fun(self):
        self.assertIn('foo', '123 foo')
    def test_save_map_transform(self):
        """
        save_map() should return 15
        """
        path = join(settings.BASE_DIR, 'box_upload/')
        scanning = Transform(path)
        scanning.setup()
        scanning.map_from_json()  # 9
        scanning.map_from_xml() # 6
        scanning.list_from_json()
        scanning.list_from_xml()

        self.assertEqual(scanning.save_map(), 15)

    def test_show_transform(self):
        """
        show() should return String contain "..."
        """
        path = join(settings.BASE_DIR, 'box_upload/')
        scanning = Transform(path)
        scanning.setup()
        scanning.map_from_json()
        scanning.map_from_xml()
        scanning.list_from_json()
        scanning.list_from_xml()
        scanning.save_map()
        self.assertIn("Scan with OOP success:",scanning.show())