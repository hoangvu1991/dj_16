from django.conf.urls import patterns, include, url

from django.contrib import admin
from inventory import views

from django.views.i18n import javascript_catalog
from django.conf.urls.i18n import i18n_patterns
from django.utils.translation import activate

from inventory.models import Item
# from rest_framework import routers, serializers, viewsets
# from rest_framework import routers

admin.autodiscover()

js_info_dict = {
    'packages': ('inventory',),
}


# Serializers define the API representation.
# class ItemSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = Item
#         fields = ('url', 'title', 'description', 'amount')

# ViewSets define the view behavior.
# class ItemViewSet(viewsets.ModelViewSet):
#     queryset = Item.objects.all()
#     serializer_class = ItemSerializer

# Routers provide an easy way of automatically determining the URL conf.
# router = routers.DefaultRouter()
# router = routers.SimpleRouter()
# router.register(r'items', ItemViewSet)

from inventory.views import HomePageView
from inventory.views import MyView
from inventory.dispatch_view import dispatch_view

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'dj_16.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    # Class-based-views
    url(r'^home/', HomePageView.as_view(), name='home'),
    url(r'^about/', MyView.as_view()),
    # url(r'^about2/', MyView()),
    url(r'^about2/', dispatch_view(MyView()), name='About2'),
    url(r'^$', dispatch_view(views.index), name='index'),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^item/(?P<id>\d+)/',views.item_detail, name='item_detail'),
    url(r'^test/', include('inventory.urls')),
    url(r'^item_add/', views.item_add, name='item_add'),
    url(r'^item_add2/', views.item_add2, name='item_add2'),
    url(r'^clean_email/', views.clean_email, name='clean_email'),
    
    url(r'^save_item', views.save_item, name='save'),
    url(r'^thanks/', views.thanks, name='thanks'),
    url(r'^parser/', views.parser, name='parser'),
    url(r'^parsing/', views.parsing, name='parsing'),
    url(r'^scan/', views.scan, name='scan'),
    url(r'^scan_opp/', views.scan_opp, name='scan_opp'),
    # url(r'^list_from_xlsx/', views.list_from_xlsx, name='list_from_xlsx'),
    url(r'^list_from_html/', views.list_from_html, name='list_from_html'),

    url(r'^authen/', views.authen, name='authen'),

    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'inventory/login.html'}),
    url(r'^accounts/profile/', views.profile_see, name='profile_see'),
    url(r'^accounts/check_login/', views.check_login, name='check_login'),
    # url(r'^my_view/(?P<m>\d+)/(?P<d>\d+)/', views.my_view, name='my_view'),
    # (r'^jsi18n/$', 'django.views.i18n.javascript_catalog', js_info_dict),
    # url(r'^', include(router.urls)),
    # url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    url(r'^test_ajax/', views.test_ajax, name='test_ajax'),
    url(r'^ajax_json/', views.ajax_json, name='ajax_json'),
    url(r'^test_try/(?P<x>\d+)/', views.test_try, name='test_try'),
    url(r'^test_try2/(?P<x>\d+)/', views.test_try2, name='test_try2')
    )

# urlpatterns += router.urls

urlpatterns += i18n_patterns('',
    # url(r'^about/$', 'about.view', name='about'),
    url(r'^my_view/', views.my_view, name='my_view'),
    url(r'^jsi18n/$', javascript_catalog, js_info_dict),
)