"""
Django settings for dj_16 project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
from os.path import join
import os
import sys
import logging
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'al)n4fh1kxv0hw7z453+v&2vn5p#4=-lka&2l@t5drg@t(@il^'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'inventory',
    'south',
    'debug_toolbar',
    # 'rest_framework'
)

# INTERNAL_IPS = ('127.0.0.1:8001',)
# INTERNAL_IPS = ('127.0.0.1','10.20.1.3','0.0.0.0')
INTERNAL_IPS = ('::ffff:10.20.1.1',)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)
# print ">>>>>>>>>>>>>>>>",os.path.join(BASE_DIR, 'locale')

ROOT_URLCONF = 'dj_16.urls'

WSGI_APPLICATION = 'dj_16.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        #'ENGINE': 'django.db.backends.sqlite3',
        #'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'dj_16',
        'USER': 'root',
        'PASSWORD': '',
        # 'PASSWORD': 'jDm3d1@',
        # 'HOST': '127.0.0.1',   # Or an IP Address that your DB is hosted on
        'HOST': '',   # Or an IP Address that your DB is hosted on
        'PORT': '',
        'OPTIONS': {
            'init_command': 'SET storage_engine=INNODB',
        }
    }
}

TEMPLATE_DIRS = (
   join(BASE_DIR, 'templates'),
   # os.path.join(os.path.dirname(__file__),'templates'),
)
# print "TEMPLATE_DIRS 1",join(BASE_DIR, 'templates') # /var/www/django/dj_16/templates
# print "TEMPLATE_DIRS 2",os.path.join(os.path.dirname(__file__),'templates') # /var/www/django/dj_16/dj_16/templates
# logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
# logging.warning('Watch out!...........') # will print a message to the console
# logging.info('I told you so .........') # will not print anything
# logging.info("SETTING............")


# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
STATICFILES_DIRS = (
    # os.path.join(BASE_DIR, 'dj_16', 'static'),
    join(BASE_DIR, 'static'),
)



DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
    'SHOW_TEMPLATE_CONTEXT': True,
}

# def show_toolbar(request):
#     return True
# SHOW_TOOLBAR_CALLBACK = show_toolbar

# DEBUG_TOOLBAR_PANELS = [
#     'debug_toolbar.panels.versions.VersionsPanel',
#     'debug_toolbar.panels.timer.TimerPanel',
#     'debug_toolbar.panels.settings.SettingsPanel',
#     'debug_toolbar.panels.headers.HeadersPanel',
#     'debug_toolbar.panels.request.RequestPanel',
#     'debug_toolbar.panels.sql.SQLPanel',
#     'debug_toolbar.panels.staticfiles.StaticFilesPanel',
#     'debug_toolbar.panels.templates.TemplatesPanel',
#     'debug_toolbar.panels.cache.CachePanel',
#     'debug_toolbar.panels.signals.SignalsPanel',
#     'debug_toolbar.panels.logging.LoggingPanel',
#     'debug_toolbar.panels.redirects.RedirectsPanel',
# ]

# REST_FRAMEWORK = {
#     # Use Django's standard `django.contrib.auth` permissions,
#     # or allow read-only access for unauthenticated users.
#     'DEFAULT_PERMISSION_CLASSES': ['rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly',],
#     # 'DEFAULT_PERMISSION_CLASSES':  ('rest_framework.permissions.IsAdminUser',),
#     'PAGE_SIZE': 10
# }