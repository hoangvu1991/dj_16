window.onload = function () {
    var on_mouse_over = function() {
        this.className = this.className + " hovered";
    };
    var on_mouse_out = function() {
        this.className = this.className.replace( /(?:^|\s) hovered(?!\S)/g , '');
    }
    var items = document.getElementsByTagName('li');
    for(var i = 0; i < items.length; i++) {
        items[i].addEventListener('mouseover', on_mouse_over);
        items[i].addEventListener('mouseout', on_mouse_out);
    };
    $("#submit").on("click",function(event){
        event.preventDefault();
        $.ajax({
          method: "POST",
          url: "/save_item",
          data: { title: $("#id_title").val(),
                  description: $("#id_description").val(),
                  amount: $("#id_amount").val(),
                  field7: $("#id_field7").val(),
                  field4_select: $("#id_field4_select").val(),
                  csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
                }
        })
          .done(function( msg ) {
            alert( "Result: " + msg.result + "\n Message: " +  msg.message);
          });
    })
};
